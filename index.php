<?
    /* http://simplehtmldom.sourceforge.net/manual.htm */
    include('simple_html_dom.php');
    $url = '';

    if(!empty($_POST)){


        $url = 'http://'.$_POST['url'];


        $html = file_get_html($url);

        if(empty($html)){
            echo 'Invalid url or no images found?';
        }else{
            $images = $html->find('img');

            foreach($images as &$image){
                if( strpos($image->src, 'http') === FALSE){
                    $image->src = $url.$image->src;
                }
            }
        }



        //$html->clear();
    }

    if(!empty($_GET)){
        $image = urldecode($_GET['image_url']);
    }

?>
<html>
<head>
    <title>Scraper demo</title>
    <link href="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.1/css/bootstrap-combined.min.css" rel="stylesheet">
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>

    <script src="image-picker/image-picker.js" type="text/javascript" charset="utf-8"></script>
    <link rel="stylesheet" href="image-picker/image-picker.css">

    </style>

    <script>
        $(document).ready(function(){

            /* http://rvera.github.io/image-picker/ */
            $("select").imagepicker();
        })
    </script>

</head>
<body>

    <div class="container">

        <? if(!empty($image)){ ?>
            <img src="<?= $image ?>">

        <? } ?>

        <form action="" method="post" class="form-search well">
            <div class="input-prepend input-append">
                <span class="add-on">http://</span>
                <input type="text" name="url" value="<?= str_replace('http://', '', $url) ?>" placeholder="" class="span4">
                <button type="submit" class="btn">Scrape</button>
            </div>

        </form>


        <? if(!empty($images)){ ?>
            <form action="" method="get" accept-charset="utf-8">
                <input type="hidden" name="site_url" value="<?= $url ?>">
                <select name="image_url" class="image-picker">

                    <? $i=0;foreach($images as $image){ ?>
                        <option data-img-src="<?= $image->src ?>" value="<?= urlencode($image->src) ?>"><?= $image->alt ?></option>
                    <?$i++;} ?>

                </select>
                <input type="submit" value="Select" class="btn btn-primary">
            </form>
        <? }else{ ?>

        <? } ?>

    </div>
</body>
</html>